FROM openjdk:8-jre-alpine

WORKDIR /app

ADD openshift.jar .

CMD ["java", "-jar", "openshift.jar"]